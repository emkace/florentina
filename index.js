if (process.argv.length < 3) {
    console.log("Error: No token specified! Exiting...");
    return;
} else {
    const timeLocaleOptions = { year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric" };
    const Discord = require("discord.js");
    const client = new Discord.Client();

    const TwitchClient = require("twitch").default;
    const WebHookListener = require("twitch-webhooks").default;

    const moment = require("moment");
    const express = require("express");
    const bodyParser = require("body-parser");

    const fs = require("fs");
    const botConfig = JSON.parse(fs.readFileSync("../config/twg/config.json"));

    if (botConfig.prefix === undefined) {
        console.log("No Prefix specified! Exiting...");
        process.exit(1);
    }

    const updateCache = async () => {
        if (botConfig.reactionChannel && botConfig.reactionMessage) {
            const reactionChannel = await client.channels.fetch(botConfig.reactionChannel);
            reactionChannel.messages.fetch(botConfig.reactionMessage);
        }
    };

    const twitchConfig = JSON.parse(fs.readFileSync("../config/twg/twitch.json"));

    const twitchClient = TwitchClient.withCredentials(
        twitchConfig.clientId,
        twitchConfig.accessToken,
        undefined,
        {
            clientSecret: twitchConfig.clientSecret,
            refreshToken: twitchConfig.refreshToken,
            expiry: twitchConfig.expiry === null ? null : new Date(twitchConfig.expiry),
            onRefresh: async ({ accessToken, refreshToken, expiryDate }) => {
                twitchConfig.accessToken = accessToken;
                twitchConfig.refreshToken = refreshToken;
                twitchConfig.expiry = expiryDate;
                fs.writeFileSync("../config/twg/twitch.json", JSON.stringify(twitchConfig, null, 4));
            }
        }
    );

    let deletionTimeout;
    let updateImageInterval;

    const updateBotConfig = () => {
        fs.writeFileSync("../config/twg/config.json", JSON.stringify(botConfig, null, 4));
    };

    const setupInstagram = () => {
        if (botConfig.instagramChannel && botConfig.instagramRole) {
            const app = express();
            app.use(bodyParser.json());
            app.listen("2002", () => {
                console.log("Server is running on port 2002 at /instagram");
            });

            app.post("/instagram", async (req, res) => {
                const channel = await client.channels.fetch(botConfig.instagramChannel);
                const response = {
                    content: "<@&" + botConfig.instagramRole + ">",
                    embed: {
                        title: req.body.type === "image" ? "Neues Bild auf Instagram" : "Neues Video auf Instagram",
                        url: req.body.url,
                        description: req.body.caption,
                        color: 12670347,
                        image: {
                            url: req.body.thumbnail
                        },
                        timestamp: moment(req.body.timestamp, "MMM DD, YYYY at hh:mmA").format("YYYY-MM-DDTHH:MM:SS.MSSZ")
                    }
                };
                channel.send(response);
                res.status(200).end();
            });
        }
    };

    const setupInterval = async () => {
        updateImageInterval = setInterval(async () => {
            if (botConfig.notificationChannel && botConfig.notificationMessage) {
                const channel = await client.channels.fetch(botConfig.notificationChannel);
                const notification = await channel.messages.fetch(botConfig.notificationMessage);
                if (notification.embeds.length > 0) {
                    const oldEmbed = notification.embeds[0];
                    if (oldEmbed.image) {
                        const baseImageUrl = oldEmbed.image.url.substring(0, oldEmbed.image.url.indexOf("?now="));
                        oldEmbed.setImage(baseImageUrl + "?now=" + Date.now());
                        notification.edit({ content: notification.content, embed: oldEmbed });
                    }
                }
            } else {
                clearInterval(updateImageInterval);
            }
        }, 600000);
    };

    const setupOfflineMessage = async () => {
        if (
            botConfig.notificationChannel &&
            !botConfig.notificationMessage &&
            !botConfig.offlineMessage &&
            botConfig.reactionChannel &&
            botConfig.notificationRole
        ) {
            const notificationChannel = await client.channels.fetch(botConfig.notificationChannel);
            const notificationRole = await notificationChannel.guild.roles.fetch(botConfig.notificationRole);
            const reactionChannel = await client.channels.fetch(botConfig.reactionChannel);
            const embed = {
                "title": "Livestream offline! Hier klicken um den letzten Stream anzuschauen",
                "description":
                    "**Stream status**:\n🔴 **Offline**\n" +
                    "\n" +
                    "Letzten Stream verpasst?\n" +
                    "[Hier](https://www.twitch.tv/thewastedgirl/videos) kannst du dir die VODs vom letzten Stream anschauen!",
                "url": "https://twitch.tv/thewastedgirl",
                "color": 13632027,
                "timestamp": Date.now(),
                "footer": {
                    "icon_url": client.user.displayAvatarURL()
                },
                "author": {
                    "name": client.user.username,
                    "icon_url": client.user.displayAvatarURL()
                },
                "fields": [
                    {
                        "name": "Benachrichtigungen einschalten!",
                        "value":
                            "Sobald ein Stream läuft erhälst du hier\n" +
                            "über die Rolle " + notificationRole.toString() + " eine Benachrichtigung!\n" +
                            "\n" +
                            "Falls du die Rolle " + notificationRole.toString() + " noch nicht hast,\n" +
                            "kannst du diese in der " + reactionChannel.toString() + " erhalten!"
                    }
                ]
            };
            const offlineMessage = await notificationChannel.send({ embed: embed });
            botConfig.offlineMessage = offlineMessage.id;
            updateBotConfig();
        }
    };

    const setupListener = async () => {
        const listener = await WebHookListener.create(twitchClient, {
            hostName: "emkace.de",
            port: 2001
        });

        listener.listen();

        // eslint-disable-next-line no-unused-vars
        const subscription = await listener.subscribeToStreamChanges(twitchConfig.streamer, async (stream) => {
            if (stream) {
                if (botConfig.notificationChannel !== undefined) {
                    clearTimeout(deletionTimeout);
                    const channel = await client.channels.fetch(botConfig.notificationChannel);
                    if (botConfig.notificationMessage === null) {
                        if (channel.type === "text") {
                            const offlineMessage = await channel.messages.fetch(botConfig.offlineMessage);
                            await offlineMessage.delete();
                            botConfig.offlineMessage = null;
                            const embed = {
                                "title": `${stream.userDisplayName} ist live!`,
                                "url": `https://twitch.tv/${stream.userDisplayName}`,
                                "color": 9442302,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "image": {
                                    "url": `https://static-cdn.jtvnw.net/previews-ttv/live_user_${stream.userDisplayName.toLowerCase()}-1920x1080.jpg?now=${Date.now()}`
                                },
                                "author": {
                                    "name": stream.userDisplayName,
                                    "url": `https://twitch.tv/${stream.userDisplayName}`,
                                    "icon_url": (await stream.getUser()).profilePictureUrl
                                },
                                "fields": [
                                    {
                                        "name": "Titel",
                                        "value": stream.title
                                    },
                                    {
                                        "name": "Spiel",
                                        "value": (await stream.getGame()).name
                                    }
                                ]
                            };
                            const notification = await channel.send({ content: (await channel.guild.roles.fetch(botConfig.notificationRole)).toString(), embed: embed });
                            botConfig.notificationMessage = notification.id;
                            updateBotConfig();
                        }
                    } else {
                        const notification = await channel.messages.fetch(botConfig.notificationMessage);
                        const embed = {
                            "title": `${stream.userDisplayName} ist live!`,
                            "url": `https://twitch.tv/${stream.userDisplayName}`,
                            "color": 9442302,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "image": {
                                "url": `https://static-cdn.jtvnw.net/previews-ttv/live_user_${stream.userDisplayName.toLowerCase()}-1920x1080.jpg?now=${Date.now()}`
                            },
                            "author": {
                                "name": stream.userDisplayName,
                                "url": `https://twitch.tv/${stream.userDisplayName}`,
                                "icon_url": (await stream.getUser()).profilePictureUrl
                            },
                            "fields": [
                                {
                                    "name": "Titel",
                                    "value": stream.title
                                },
                                {
                                    "name": "Spiel",
                                    "value": (await stream.getGame()).name
                                }
                            ]
                        };
                        await notification.edit({ content: (await channel.guild.roles.fetch(botConfig.notificationRole)).toString(), embed: embed });
                    }
                    clearInterval(updateImageInterval);
                    setupInterval();
                    setPresence(1, `https://twitch.tv/${stream.userDisplayName}`);
                }
            } else {
                deletionTimeout = setTimeout(async () => {
                    if (botConfig.notificationChannel !== undefined && botConfig.notificationMessage) {
                        const channel = await client.channels.fetch(botConfig.notificationChannel);
                        if (channel.type === "text") {
                            const notification = await channel.messages.fetch(botConfig.notificationMessage);
                            notification.delete().then(() => {
                                botConfig.notificationMessage = null;
                                setupOfflineMessage();
                                updateBotConfig();
                                clearInterval(updateImageInterval);
                            });
                        }
                        setPresence(0);
                    }
                }, 600000);
            }
        });
    };

    const updateReactionMessage = async () => {
        if (
            botConfig.reactionChannel && botConfig.reactionRoles) {
            const channel = await client.channels.fetch(botConfig.reactionChannel);
            if (channel.type === "text") {
                let reactionMessage;
                if (botConfig.reactionMessage) {
                    reactionMessage = await channel.messages.fetch(botConfig.reactionMessage);
                } else {
                    const loadingLines = ["Generating terrain", "Loading Lines in 2020 LUL", "Switching sides", "Dispatching carrier pigeons", "Preparing Final Form.", "Activating Witch Time", "Spinning to win", "Entering cheat codes", "Rushing B", "Pressing random buttons", "Resetting Run", "Removing pen from pineapple", "Caution: Contents Spicy", "Clicking circles (to the beat!)", "Building Lore", "Scaling Bananas", "l o a d i n g a e s t h e t i c s"];
                    reactionMessage = await channel.send({ content: loadingLines[Math.floor(Math.random() * loadingLines.length)] + "..." });
                    botConfig.reactionMessage = reactionMessage.id;
                    updateBotConfig();
                }
                let embed;
                if (botConfig.reactionRoles.length > 0) {
                    const fields = botConfig.reactionRoles.map(async (reactionRole) => {
                        const role = await channel.guild.roles.fetch(reactionRole.id);
                        const existingReactionIndex = reactionMessage.reactions.cache.find(messageReaction => {
                            if (messageReaction.emoji.id) {
                                messageReaction.emoji.id === reactionRole.emote;
                            } else {
                                messageReaction.emoji.name === reactionRole.emote;
                            }
                        });
                        let newReaction = null;
                        if (existingReactionIndex === undefined) {
                            newReaction = await reactionMessage.react(reactionRole.emote);
                        }
                        return newReaction === null ? { name: role.name, value: "Wähle das " + reactionRole.emote + " Emote um die Rolle " + role.toString() + " zu erhalten" } : { name: role.name, value: "Wähle das " + newReaction.emoji.toString() + " Emote um die Rolle " + role.toString() + " zu erhalten" };
                    });
                    embed = {
                        "title": "Rollenvergabe",
                        "description": "Um eine Rolle deiner Wahl zu erhalten musst du einfach mit dem entsprechenden Emote reagieren!\nFalls du die Rolle nicht mehr möchtest kannst du durch das Entfernen der Reaktion die Rolle wieder loswerden!",
                        "color": 16777215,
                        "timestamp": Date.now(),
                        "footer": {
                            "icon_url": client.user.displayAvatarURL()
                        },
                        "author": {
                            "name": client.user.username,
                            "icon_url": client.user.displayAvatarURL()
                        },
                        "fields": await Promise.all(fields)
                    };
                } else {
                    embed = {
                        "title": "Rollenvergabe",
                        "description": "Es gibt aktuell keine Rollen, die du dir selbst vergeben kannst!",
                        "color": 16777215,
                        "timestamp": Date.now(),
                        "footer": {
                            "icon_url": client.user.displayAvatarURL()
                        },
                        "author": {
                            "name": client.user.username,
                            "icon_url": client.user.displayAvatarURL()
                        }
                    };
                }
                reactionMessage.edit({ content: "", embed: embed });
            }
        }
    };

    const deleteReactionEmote = async (emote) => {
        if (botConfig.reactionChannel && botConfig.reactionRoles && botConfig.reactionMessage) {
            const channel = await client.channels.fetch(botConfig.reactionChannel);
            if (channel.type === "text") {
                const reactionMessage = await channel.messages.fetch(botConfig.reactionMessage);
                reactionMessage.reactions.cache.get(emote).remove();
            }
        }
    };

    const sendInfo = async (message, content) => {
        message.delete();
        // eslint-disable-next-line no-case-declarations
        const reply = await message.channel.send(content);
        await reply.react("🗑️");
        // eslint-disable-next-line no-case-declarations
        const filter = (reaction, user) => {
            return reaction.emoji.name === "🗑️" && user.id === message.author.id;
        };
        // eslint-disable-next-line no-case-declarations
        const collector = reply.createReactionCollector(filter, { time: 60000 });
        collector.on("collect", () => {
            reply.delete();
        });

        collector.on("end", (collected) => {
            if (collected.size < 1) {
                reply.reactions.removeAll();
            }
        });
        return;
    };

    const setPresence = (status, url) => {
        switch (status) {
            case 0:
                client.user.setActivity("!help", { type: "LISTENING" });
                break;
            case 1:
                client.user.setActivity("!help", { url: url, type: "STREAMING" });
                break;
        }
    };

    client.on("ready", () => {
        updateCache();
        setupListener();
        setupInstagram();
        setupOfflineMessage();
        if (botConfig.notificationChannel && botConfig.notificationMessage) setupInterval();
        setPresence(0);
        console.log(`Logged in as ${client.user.tag}`);
    });

    client.on("message", async (message) => {
        if (message.author.bot) return;
        if (message.member === null) return;
        if (message.content.startsWith(botConfig.prefix)) {
            const command = message.content.substring(1).split(" ");
            if (command.length < 1) return;
            switch (command[0].toLowerCase()) {
                case "clear":
                    console.log(`Clear called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    message.delete();
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_MESSAGES)) {
                        if (command.length === 2) {
                            const amount = parseInt(command[1]);
                            if (!isNaN(amount)) {
                                if (amount > 1 && amount < 101) {
                                    message.channel.bulkDelete(amount).then(async (messages) => {
                                        const embed = {
                                            "title": "Clear erfolgreich!",
                                            "description": `Erfolgreich ${messages.size} Nachrichten gelöscht!`,
                                            "color": 5301186,
                                            "timestamp": Date.now(),
                                            "footer": {
                                                "icon_url": client.user.displayAvatarURL()
                                            },
                                            "author": {
                                                "name": message.member.displayName,
                                                "icon_url": message.author.displayAvatarURL()
                                            }
                                        };
                                        const reply = await message.channel.send({ embed: embed });
                                        reply.delete({ timeout: 20000 });
                                    });
                                    return;
                                }
                            }
                        }
                        const embed = {
                            "title": "Clear",
                            "description": "Um die letzten x Nachrichten zu löschen musst du `" + botConfig.prefix + "clear x` in den entsprechenden Channel schreiben, wobei x eine Zahl zwischen 2 und 100 sein muss aufgrund von Discords Limitierungen!",
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                        return;
                    } else {
                        return;
                    }
                case "addreactionrole":
                    console.log(`AddReactionRole called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_ROLES)) {
                        message.delete();
                        let embed = {
                            "title": "AddReactionRole",
                            "description": "Um der Rollenvergabe ein neue Rolle hinzuzufügen musst du `" + botConfig.prefix + "addReactionRole @role` eingeben!",
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.roles.size > 0) {
                            embed = {
                                "title": "AddReactionRole",
                                "description": "Bitte reagiere mit dem entsprechenden Emote um dieses " + message.mentions.roles.first().toString() + " zuzuteilen.\nFalls du den Vorgang abbrechen möchtest reagiere mit :x:",
                                "color": 16098851,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                            const collectorMessage = await message.channel.send({ embed: embed });
                            collectorMessage.react("❌");
                            const filter = (reaction, user) => {
                                return user.id === message.author.id;
                            };
                            const collector = collectorMessage.createReactionCollector(filter, { max: 1, time: 60000 });
                            collector.on("collect", async (reaction) => {
                                collectorMessage.delete();
                                if (reaction.emoji.name === "❌") {
                                    embed = {
                                        "title": "AddReactionRole",
                                        "description": "Vorgang abgebrochen!",
                                        "color": 13632027,
                                        "timestamp": Date.now(),
                                        "footer": {
                                            "icon_url": client.user.displayAvatarURL()
                                        },
                                        "author": {
                                            "name": message.member.displayName,
                                            "icon_url": message.author.displayAvatarURL()
                                        }
                                    };
                                } else {
                                    embed = {
                                        "title": "AddReactionRole",
                                        "description": message.mentions.roles.first().toString() + " verwendet nun " + reaction.emoji.toString(),
                                        "color": 5301186,
                                        "timestamp": Date.now(),
                                        "footer": {
                                            "icon_url": client.user.displayAvatarURL()
                                        },
                                        "author": {
                                            "name": message.member.displayName,
                                            "icon_url": message.author.displayAvatarURL()
                                        }
                                    };
                                    const existingRoleIndex = botConfig.reactionRoles.findIndex(role => role.id == message.mentions.roles.first().id);
                                    if (existingRoleIndex > -1) {
                                        if (reaction.emoji.id) {
                                            botConfig.reactionRoles[existingRoleIndex].emote = reaction.emoji.id;
                                        } else {
                                            botConfig.reactionRoles[existingRoleIndex].emote = reaction.emoji.name;
                                        }
                                    } else {
                                        if (reaction.emoji.id) {
                                            botConfig.reactionRoles.push({ id: message.mentions.roles.first().id, emote: reaction.emoji.id });
                                        } else {
                                            botConfig.reactionRoles.push({ id: message.mentions.roles.first().id, emote: reaction.emoji.name });
                                        }
                                    }
                                    updateBotConfig();
                                    updateReactionMessage();
                                    const reply = await message.channel.send({ embed: embed });
                                    reply.delete({ timeout: 20000 });
                                }
                            });
                        } else {
                            const reply = await message.channel.send({ embed: embed });
                            reply.delete({ timeout: 20000 });
                        }
                    }
                    return;
                case "removereactionrole":
                    console.log(`RemoveReactionRole called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_ROLES)) {
                        message.delete();
                        let embed = {
                            "title": "RemoveReactionRole",
                            "description": "Um eine Rolle aus der Rollenvergabe zu entfernen musst du `" + botConfig.prefix + "removeReactionRole @role` schreiben.",
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.roles.size > 0) {
                            const existingRoleIndex = botConfig.reactionRoles.findIndex(role => role.id == message.mentions.roles.first().id);
                            if (existingRoleIndex > -1) {
                                deleteReactionEmote(botConfig.reactionRoles[existingRoleIndex].emote);
                                botConfig.reactionRoles.splice(existingRoleIndex, 1);
                                updateBotConfig();
                                updateReactionMessage();
                                embed = {
                                    "title": "RemoveReactionRole",
                                    "description": "Die Rolle " + message.mentions.roles.first().toString() + " wurde entfernt!",
                                    "color": 5301186,
                                    "timestamp": Date.now(),
                                    "footer": {
                                        "icon_url": client.user.displayAvatarURL()
                                    },
                                    "author": {
                                        "name": message.member.displayName,
                                        "icon_url": message.author.displayAvatarURL()
                                    }
                                };
                            } else {
                                embed = {
                                    "title": "RemoveReactionRole",
                                    "description": "Diese Rolle ist nicht in der Reaction-Liste eingetragen!",
                                    "color": 13632027,
                                    "timestamp": Date.now(),
                                    "footer": {
                                        "icon_url": client.user.displayAvatarURL()
                                    },
                                    "author": {
                                        "name": message.member.displayName,
                                        "icon_url": message.author.displayAvatarURL()
                                    }
                                };
                            }
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                    }
                    return;
                case "setinstagramchannel":
                    console.log(`setInstagramChannel called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_CHANNELS)) {
                        message.delete();
                        let embed = {
                            "title": "SetInstagramChannel",
                            "description": "Um den Channel für die Instagram-Benachrichtigungen festzulegen musst du `" + botConfig.prefix + "setInstagramChannel #channelName` schreiben.\n" +
                                "\n" +
                                "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                                botConfig.prefix + "setInstagramChannel " + message.channel.toString(),
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.channels.size > 0) {
                            const wasExpressRunning = botConfig.instagramChannel !== null && botConfig.instagramChannel !== undefined && botConfig.instagramRole !== null && botConfig.instagramRole !== undefined;
                            botConfig.instagramChannel = message.mentions.channels.first().id;
                            updateBotConfig();
                            if (!wasExpressRunning) setupInstagram();
                            embed = {
                                "title": "SetInstagramChannel",
                                "description": "Die Instagram-Benachrichtigungen befinden sich ab sofort in " + message.mentions.channels.first().toString() + "!",
                                "color": 5301186,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                    }
                    return;
                case "setinstagramrole":
                    console.log(`SetInstagramRole called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_ROLES)) {
                        message.delete();
                        let embed = {
                            "title": "SetInstagramRole",
                            "description": "Um bei Instagram-Posts eine bestimmte Rolle zu benachrichten musst du `" + botConfig.prefix + "setInstagramRole @role` schreiben.",
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.roles.size > 0) {
                            const wasExpressRunning = botConfig.instagramChannel !== null && botConfig.instagramChannel !== undefined && botConfig.instagramRole !== null && botConfig.instagramRole !== undefined;
                            botConfig.instagramRole = message.mentions.roles.first().id;
                            updateBotConfig();
                            if (!wasExpressRunning) setupInstagram();
                            embed = {
                                "title": "SetNotificationRole",
                                "description": "Bei neuen Instagram-Posts wird ab sofort die Rolle " + message.mentions.roles.first().toString() + " gepingt!",
                                "color": 5301186,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                        return;
                    }
                    return;
                case "setreactionchannel":
                    console.log(`setReactionChannel called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_CHANNELS)) {
                        message.delete();
                        let embed = {
                            "title": "SetReactionChannel",
                            "description": "Um den Channel für die Rollenvergabe festzulegen musst du `" + botConfig.prefix + "setReactionChannel #channelName` schreiben.\n" +
                                "\n" +
                                "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                                botConfig.prefix + "setReactionChannel " + message.channel.toString(),
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.channels.size > 0) {
                            botConfig.reactionChannel = message.mentions.channels.first().id;
                            updateBotConfig();
                            updateReactionMessage();
                            embed = {
                                "title": "SetReactionChannel",
                                "description": "Die Rollenvergabe befindet sich ab sofort in " + message.mentions.channels.first().toString() + "!",
                                "color": 5301186,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                    }
                    return;
                case "settwitchchannel":
                    console.log(`SetTwitchChannel called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_CHANNELS)) {
                        message.delete();
                        let embed = {
                            "title": "SetTwitchChannel",
                            "description": "Um Streambenachrichtigungen zukünftig in einem Channel zu erhalten musst du `" + botConfig.prefix + "setTwitchChannel #channelName` schreiben.\n" +
                                "\n" +
                                "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                                botConfig.prefix + "setTwitchChannel " + message.channel.toString(),
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.channels.size > 0) {
                            botConfig.notificationChannel = message.mentions.channels.first().id;
                            updateBotConfig();
                            embed = {
                                "title": "SetTwitchChannel",
                                "description": "Streambenachrichtigungen werden ab sofort in " + message.mentions.channels.first().toString() + " versendet!",
                                "color": 5301186,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                    }
                    return;
                case "setnotificationrole":
                    console.log(`SetNotificationRole called by ${message.author.tag} at ${new Date().toLocaleDateString("de-DE", timeLocaleOptions)}`);
                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_ROLES)) {
                        message.delete();
                        let embed = {
                            "title": "SetNotificationRole",
                            "description": "Um bei Streambenachrichtigungen eine bestimmte Rolle zu benachrichten musst du `" + botConfig.prefix + "setNotificationRole @role` schreiben.",
                            "color": 13632027,
                            "timestamp": Date.now(),
                            "footer": {
                                "icon_url": client.user.displayAvatarURL()
                            },
                            "author": {
                                "name": message.member.displayName,
                                "icon_url": message.author.displayAvatarURL()
                            }
                        };
                        if (message.mentions.roles.size > 0) {
                            botConfig.notificationRole = message.mentions.roles.first().id;
                            updateBotConfig();
                            embed = {
                                "title": "SetNotificationRole",
                                "description": "Bei Streambenachrichtigungen wird ab sofort die Rolle " + message.mentions.roles.first().toString() + " gepingt!",
                                "color": 5301186,
                                "timestamp": Date.now(),
                                "footer": {
                                    "icon_url": client.user.displayAvatarURL()
                                },
                                "author": {
                                    "name": message.member.displayName,
                                    "icon_url": message.author.displayAvatarURL()
                                }
                            };
                        }
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 20000 });
                        return;
                    }
                    return;
                case "test":
                    if (botConfig.notificationChannel && botConfig.notificationMessage) {
                        const channel = await client.channels.fetch(botConfig.notificationChannel);
                        const notification = await channel.messages.fetch(botConfig.notificationMessage);
                        notification.delete();
                    }
                    return;
                case "instagram":
                    sendInfo(message, "Mein Instagram:\nhttps://www.instagram.com/thewastedgirl.official/");
                    return;
                case "steam":
                    sendInfo(message, "Mein Steam:\nhttps://steamcommunity.com/id/thewastedgirl");
                    return;
                case "switch":
                    sendInfo(message, "Mein Switch Freundescode:\n**SW-8107-0117-8423**");
                    return;
                case "xbox":
                    sendInfo(message, "Mein Xbox-Gamertag:\n**Thewastedgirl**");
                    return;
                case "dangerouslypurge":
                    if (message.member.hasPermission([Discord.Permissions.FLAGS.MANAGE_MESSAGES, Discord.Permissions.FLAGS.MANAGE_CHANNELS])) {
                        message.delete();
                        if (message.mentions.channels.size > 0) {
                            const collectorMessage = await message.channel.send("Möchtest du wirklich alle Nachrichten aus " + message.mentions.channels.first().toString() + " entfernen? Diese Option sollte mit Vorsicht verwendet werden!");
                            await collectorMessage.react("✔");
                            await collectorMessage.react("❌");
                            const filter = (reaction, user) => {
                                return (reaction.emoji.name === "✔" || reaction.emoji.name === "❌") && user.id === message.author.id;
                            };
                            // eslint-disable-next-line no-case-declarations
                            const collector = collectorMessage.createReactionCollector(filter, { time: 60000 });
                            collector.on("collect", async (reaction) => {
                                collector.stop();
                                if (reaction.emoji.name === "✔") {
                                    const toDelete = await message.mentions.channels.first().messages.fetch({ limit: 100 });
                                    let timeout = 2000;
                                    toDelete.forEach(async (messageToDelete) => {
                                        setTimeout(() => {
                                            messageToDelete.delete();
                                        }, timeout);
                                    });

                                }
                            });

                            collector.on("end", () => {
                                collectorMessage.delete();
                            });
                        }
                    }
                    return;
                case "help":
                    // eslint-disable-next-line no-case-declarations
                    const embed = new Discord.MessageEmbed()
                        .setTitle("Hilfe")
                        .setDescription("Hier eine Übersicht der Befehle, die du nutzen kannst!")
                        .setColor(4886754)
                        .setTimestamp(Date.now())
                        .setAuthor(message.member.displayName, message.author.displayAvatarURL());

                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_MESSAGES))
                        embed.addField(
                            botConfig.prefix + "clear",
                            "Um die letzten x Nachrichten zu löschen musst du `" + botConfig.prefix + "clear x` in den entsprechenden Channel schreiben, wobei x eine Zahl zwischen 2 und 100 sein muss aufgrund von Discords Limitierungen!",
                            false
                        );

                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_ROLES)) {
                        embed.addField(
                            botConfig.prefix + "addReactionRole",
                            "Um der Rollenvergabe ein neue Rolle hinzuzufügen musst du `" + botConfig.prefix + "addReactionRole @role` eingeben!",
                            false
                        );
                        embed.addField(
                            botConfig.prefix + "removeReactionRole",
                            "Um eine Rolle aus der Rollenvergabe zu entfernen musst du `" + botConfig.prefix + "removeReactionRole @role` schreiben.",
                            false
                        );
                        embed.addField(
                            botConfig.prefix + "setInstagramRole",
                            "Um bei Instagram-Posts eine bestimmte Rolle zu benachrichten musst du\n`" + botConfig.prefix + "setInstagramRole @role` schreiben.",
                            false
                        );
                        embed.addField(
                            botConfig.prefix + "setNotificationRole",
                            "Um bei Streambenachrichtigungen eine bestimmte Rolle zu benachrichten musst du\n`" + botConfig.prefix + "setNotificationRole @role` schreiben.",
                            false
                        );
                    }

                    if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_CHANNELS)) {
                        embed.addField(
                            botConfig.prefix + "setInstagramChannel",
                            "Um den Channel für die Instagram-Benachrichtigungen festzulegen musst du `" + botConfig.prefix + "setInstagramChannel #channelName` schreiben.\n" +
                            "\n" +
                            "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                            botConfig.prefix + "setInstagramChannel " + message.channel.toString(),
                            false
                        );
                        embed.addField(
                            botConfig.prefix + "setReactionChannel",
                            "Um den Channel für die Rollenvergabe festzulegen musst du `" + botConfig.prefix + "setReactionChannel #channelName` schreiben.\n" +
                            "\n" +
                            "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                            botConfig.prefix + "setReactionChannel " + message.channel.toString(),
                            false
                        );
                        embed.addField(
                            botConfig.prefix + "setTwichChannel",
                            "Um Streambenachrichtigungen zukünftig in einem Channel zu erhalten musst du\n`" + botConfig.prefix + "setTwitchChannel #channelName` schreiben.\n" +
                            "\n" +
                            "Um z.B. diesen Channel zu verwenden musst du folgenden Befehl eintippen:\n" +
                            "**" + botConfig.prefix + "setTwitchChannel " + message.channel.toString() + "**",
                            false
                        );
                    }

                    embed.addField(
                        botConfig.prefix + "instagram",
                        "Gebe `" + botConfig.prefix + "instagram` ein, um einen Link zu meiner Instagram-Seite zu erhalten!",
                        false
                    );
                    embed.addField(
                        botConfig.prefix + "steam",
                        "Gebe `" + botConfig.prefix + "steam` ein, um einen Link zu meinem Steam-Profil zu erhalten!",
                        false
                    );
                    embed.addField(
                        botConfig.prefix + "switch",
                        "Gebe `" + botConfig.prefix + "switch` ein, um meinen Switch Freundescode zu erhalten!",
                        false
                    );
                    embed.addField(
                        botConfig.prefix + "xbox",
                        "Gebe `" + botConfig.prefix + "xbox` ein, um meinen Xbox-Gamertag zu erhalten!",
                        false
                    );

                    if (embed.fields.length > 0) {
                        message.delete();
                        const reply = await message.channel.send({ embed: embed });
                        reply.delete({ timeout: 60000 });
                    }
                    return;
            }
        }
    });

    client.on("messageReactionAdd", async (reaction, user) => {
        if (user.bot) return;
        if (botConfig.reactionMessage && botConfig.reactionRoles) {
            if (reaction.message.id === botConfig.reactionMessage) {
                if (botConfig.reactionRoles.length > 0) {
                    const reactionIndex = botConfig.reactionRoles.findIndex(reactionRole => reactionRole.emote === reaction.emoji.name || reactionRole.emote === reaction.emoji.id);
                    if (reactionIndex > -1) {
                        const roleToAdd = await reaction.message.guild.roles.fetch(botConfig.reactionRoles[reactionIndex].id);
                        const memberToAddTo = await reaction.message.guild.members.fetch(user.id);
                        memberToAddTo.roles.add(roleToAdd);
                    } else {
                        reaction.remove();
                    }
                }
            }
        }
    });

    client.on("messageReactionRemove", async (reaction, user) => {
        if (user.bot) return;
        if (botConfig.reactionMessage && botConfig.reactionRoles) {
            if (reaction.message.id === botConfig.reactionMessage) {
                if (botConfig.reactionRoles.length > 0) {
                    const reactionIndex = botConfig.reactionRoles.findIndex(reactionRole => reactionRole.emote === reaction.emoji.name || reactionRole.emote === reaction.emoji.id);
                    if (reactionIndex > -1) {
                        const roleToRemove = await reaction.message.guild.roles.fetch(botConfig.reactionRoles[reactionIndex].id);
                        const memberToRemoveFrom = await reaction.message.guild.members.fetch(user.id);
                        memberToRemoveFrom.roles.remove(roleToRemove);
                    } else {
                        reaction.remove();
                    }
                }
            }
        }
    });

    client.login(process.argv.pop());
}