# BartBot

Bot für Aline's Discord server.


## Requirements:

- NodeJS version 12+
- npm


## Installation:

- Clone repo
- Run `npm install`
- Run `npm start <your token here>`